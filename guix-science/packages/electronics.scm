;;; Copyright © 2024 Cayetano Santos <csantosb@inventati.org>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-science packages electronics)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (gnu packages bootstrap)
  #:use-module (gnu packages check)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages fpga)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject))

(define-public gnat
  (package
    (name "gnat")
    (version "14.2.0-1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/alire-project/GNAT-FSF-builds/"
                           "releases/download/gnat-14.2.0-1/gnat-x86_64-linux-" version
                           ".tar.gz"))
       (sha256
        (base32 "08kpd3d7si73gsm2dfp5lmrhii9k96y972sw39h1sdvhgzpkvfq6"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:strip-binaries? #f
      ;; XXX: This would check DT_RUNPATH, but patchelf populate DT_RPATH,
      ;; not DT_RUNPATH.
      #:validate-runpath? #f
      #:phases #~(modify-phases %standard-phases
                   (replace 'unpack
                     (lambda* (#:key inputs #:allow-other-keys)
                       (let ((source (assoc-ref inputs "source")))
                         (invoke "tar" "xvzf" source)
                         (chdir "gnat-x86_64-linux-14.2.0-1"))))
                   (delete 'configure)
                   (delete 'check)
                   (replace 'build
                     (lambda* (#:key inputs #:allow-other-keys)
                       (define libc
                         (assoc-ref inputs "libc"))
                       (define gcc-lib
                         (assoc-ref inputs "gcc:lib"))
                       (define ld.so
                         (search-input-file inputs
                                            #$(glibc-dynamic-linker)))
                       (define rpath
                         (string-join (list "$ORIGIN"
                                            (string-append #$output "/lib")
                                            (string-append #$output "/lib64")
                                            (string-append libc "/lib")
                                            (string-append gcc-lib "/lib"))
                                      ":"))

                       ;; patchelf procedure
                       (define (patch-elf file)
                         (make-file-writable file)

                         (unless (or (string-contains file ".so")
                                     (string-contains file ".o"))
                           (format #t "Setting RPATH on '~a'...~%" file)
                           (invoke "patchelf" "--set-rpath" rpath
                                   "--force-rpath" file))

                         (unless (or (string-contains file ".so")
                                     (string-contains file ".o"))
                           (format #t "Setting RPATH on '~a'...~%" file)
                           (invoke "patchelf" "--set-rpath" rpath
                                   "--force-rpath" file))

                         (unless (or (string-contains file ".so")
                                     (string-contains file ".o"))
                           (format #t "Setting interpreter on '~a'...~%" file)
                           (invoke "patchelf" "--set-interpreter" ld.so file)))

                       ;; patch files
                       (for-each (lambda (file)
                                   (when (elf-file? file)
                                     (patch-elf file)))
                                 (find-files "."
                                             (lambda (file stat)
                                               (eq? 'regular
                                                    (stat:type stat)))))))

                   (replace 'install
                     (lambda* _
                       (let ((bin (string-append #$output "/bin"))
                             (lib (string-append #$output "/lib"))
                             (lib64 (string-append #$output "/lib64"))
                             (libexec (string-append #$output "/libexec"))
                             (x86_64-pc-linux-gnu (string-append #$output
                                                                 "/x86_64-pc-linux-gnu")))
                         (mkdir-p #$output)
                         (copy-recursively "bin" bin)
                         (copy-recursively "lib" lib)
                         (copy-recursively "lib64" lib64)
                         (copy-recursively "libexec" libexec)
                         (copy-recursively "x86_64-pc-linux-gnu"
                                           x86_64-pc-linux-gnu)))))))
    (native-inputs (list patchelf))
    (inputs `(("gcc:lib" ,gcc-14 "lib")))
    (home-page "https://github.com/alire-project/GNAT-FSF-builds")
    (synopsis "Builds of the GNAT Ada compiler from Alire Project")
    (description
     "This package gathers GNAT binaries from FSF GCC releases of the Alire
Project.")
    (license license:gpl3+)))

(define-public ghdl-clang
  (package
    (name "ghdl-clang")
    (version "4.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/ghdl/ghdl")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1wq231slpm594qr5ad441igancxr6b0hczgql0cx2xpapmx8gx5l"))))
    (build-system gnu-build-system)
    (arguments
     (list
      ;; XXX: This would check DT_RUNPATH, but patchelf populate DT_RPATH,
      ;; not DT_RUNPATH.
      #:validate-runpath? #f
      #:out-of-source? #t
      #:phases #~(modify-phases %standard-phases
                   (replace 'configure
                     (lambda* (#:key inputs #:allow-other-keys)
                       (let ((libc (assoc-ref inputs "libc")))
                         (invoke "./configure"
                                 "--with-llvm-config"
                                 "--enable-libghdl"
                                 "--enable-synth"
                                 "--disable-gplcompat"
                                 (string-append "--prefix="
                                                #$output)))))
                   (delete 'check)
                   (replace 'build
                     (lambda* (#:key inputs #:allow-other-keys)
                       (invoke "make" "ghdl_llvm" "-j"
                               (number->string (parallel-job-count)))
                       (invoke "patchelf" "--set-interpreter"
                               (search-input-file inputs
                                                  #$(glibc-dynamic-linker))
                               "ghdl_llvm")
                       (invoke "make" "-j"
                               (number->string (parallel-job-count))))))))
    (propagated-inputs (list clang-toolchain))
    (inputs (list gnat patchelf))
    (home-page "https://github.com/ghdl/ghdl")
    (synopsis "Compiler for VHDL code using clang backend")
    (description
     "GHDL analyses, elaborates and simulates VHDL sources.  It may also be
used as an experimental synthesizer backend.")
    (license license:gpl2+)))

(define-public ghdl-yosys-plugin
  ;; The project has no releases at this point
  (let ((commit "511412f984d64ed7c46c4bdbd839f4b3c48f6fa5")
        (revision "0"))
    (package
      (name "ghdl-yosys-plugin")
      (version (git-version "0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ghdl/ghdl-yosys-plugin")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1mbg4s80sbmhgmrgh3wvjwnbg0q6ha2rsm8xgypdy7pq7bp0pc97"))))
      (build-system gnu-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (delete 'configure)
                     (delete 'check)
                     (replace 'install
                       (lambda* _
                         (install-file "ghdl.so"
                                       (string-append #$output "/lib/yosys")))))))
      (native-inputs (list ghdl-clang))
      (inputs (list tcl readline))
      (propagated-inputs (list yosys))
      (home-page "https://github.com/ghdl/ghdl-yosys-plugin")
      (synopsis "VHDL synthesis based on GHDL and Yosys")
      (description
       "This plugin provides a shared library module for Yosys to implement logical synthesis of VHDL designs.")
      (license license:gpl3+))))

(define-public python-cocotb
  (package
    (name "python-cocotb")
    (version "1.9.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/cocotb/cocotb")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "19mybnhqa2jz134jj8686310fniav5nldiq0y7kbgml81ppai87c"))))
    (build-system pyproject-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda* (#:key tests? inputs outputs #:allow-other-keys)
                      (when tests?
                        (invoke "make" "-C" "tests")))))))
    (native-inputs (list python-setuptools
                         python-wheel
                         python-pytest
                         ;; tests
                         iverilog
                         ghdl-clang))
    (propagated-inputs (list python-find-libpython))
    (home-page "https://github.com/cocotb/cocotb")
    (synopsis "Library for writing HDL testbenches in Python")
    (description "Coroutine based cosimulation testbench environment for
verifying VHDL and Verilog RTL using Python.")
    (license license:bsd-3)))

(define-public python-pyvhdlmodel
  (package
    (name "python-pyvhdlmodel")
    (version "0.30.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/VHDL/pyvhdlmodel/")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0w9wbcpa306hlv366qk6xma12z5z0c01mqcd33asg42hfjyhq1qb"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (when tests?
                        (invoke "pytest" "tests" "-vv")))))))
    (native-inputs (list python-setuptools
                         python-wheel
                         ;; testing
                         python-pytest))
    (propagated-inputs (list ghdl-clang python-pytooling))
    (home-page "https://vhdl.github.io/pyVHDLModel/")
    (synopsis "High level API for GHDL")
    (description
     "@code{pyVHDLModel} provides an unified abstract language model for VHDL
written in Python.")
    (license license:asl2.0)))
