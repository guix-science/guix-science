;;; Copyright © 2024 Julien Castelneau <julien.castelneau@inria.fr>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-science packages neuroscience)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages check)
  #:use-module (gnu packages graphviz)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages rdf)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages time)
  #:use-module (guix)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:))

(define-public python-ci-info
  (package
    (name "python-ci-info")
    (version "0.2.0")
    (home-page "https://github.com/mgxd/ci-info")
    (source
     (origin (file-name (git-file-name name version))
       (method git-fetch)
       (uri (git-reference
         (url home-page)
         (commit version)))
       (sha256
        (base32 "14hl3ddra9kiln12hgnib0a3sn353r5v85ldkf6znwn84967zysc"))))
    (build-system python-build-system)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   ;; versioneer requires git metadata, which is
                   ;; stripped from the checkout.
                   (add-after 'unpack 'fix-versioneer
                     (lambda _
                       (invoke "versioneer" "install")
                       (substitute* "setup.py"
                         (("versioneer.get_version\\(\\)")
                          (string-append "\""
                                         #$(package-version this-package) "\""))))))))
    (native-inputs (list python-versioneer))
    (synopsis "Gather continuous integration information on the fly")
    (description "It helps developers working in continuous integration (CI)
environments by providing essential information about the CI server.
It can determine if the code is running on a CI server,identify the
specific server,and detect if a pull request is being tested.")
    (license license:expat)))

(define-public python-etelemetry
  (package
    (name "python-etelemetry")
    (version "0.3.1")
    (home-page "https://github.com/sensein/etelemetry-client")
    (source
     (origin (file-name (git-file-name name version))
       (method git-fetch)
       (uri (git-reference
         (url home-page)
         (commit (string-append "v" version))))
       (sha256
        (base32 "0z40nh0jnpgg6v1kckmai7b8g7hypvh1fqhxr8j2gffnzm6bf4df"))))
    (build-system python-build-system)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   ;; versioneer requires git metadata, which is
                   ;; stripped from the checkout.
                   (add-after 'unpack 'fix-versioneer
                     (lambda _
                       (invoke "versioneer" "install")
                       (substitute* "setup.py"
                         (("versioneer.get_version\\(\\)")
                          (string-append "\""
                                         #$(package-version this-package) "\""))))))))
    (native-inputs (list python-versioneer
                         python-pytest
                         python-pytest-cov))
    (propagated-inputs (list python-requests
                             python-packaging
                             python-ci-info))
    (synopsis "Lightweight python client to communicate with the etelemetry server")
    (description "The etelemetry Python client facilitates communication with the
etelemetry server, providing version information and checking for
critical bugs in projects.  The client allows you to retrieve project
details and compare versions to identify and warn about problematic
versions.")
    (license license:asl2.0)))

(define-public python-traits
  (package
    (name "python-traits")
    (version "6.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "traits" version))
       (sha256
        (base32 "0i1gwahjy5lbrlrghfx40lhrr8lnrdllxxkfx03xxdq81jggvfx9"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-setuptools
                         python-sphinx
                         python-wheel))
    (home-page "https://docs.enthought.com/traits/")
    (synopsis "Observable typed attributes for Python classes")
    (description "The Traits library is designed to enhance object-oriented programming
in Python by providing a way to define and manage attributes of
objects more effectively.")
    (license license:bsd-3)))

(define-public python-acres
  (package
    (name "python-acres")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "acres" version))
        (sha256
          (base32 "0nzlf30d2skkz51dcn6q7qrffxz6359rxnj7i4v4k61qhfb4fra7"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-importlib-resources))
    (native-inputs (list python-flit
                         python-pytest))
    (home-page "https://github.com/nipreps/acres/")
    (synopsis "Access resources on your terms")
    (description "This module provides simple, consistent access to package resources.")
    (license license:asl2.0)))

(define-public python-nipype
  (package
    (name "python-nipype")
    (version "1.9.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "nipype" version))
        (sha256
          (base32 "02vd97svg4l8rvpvwa8k42nypr6r81g283716qsv8sp8sa788134"))))
    (build-system pyproject-build-system)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   ;; Some tests contains a hardcoded absolute path to
                   ;; bash.
                   (add-after 'unpack 'fix-path
                     (lambda* (#:key native-inputs inputs #:allow-other-keys)
                       (substitute* "nipype/pipeline/engine/tests/test_nodes.py"
                         (("/bin/bash")
                          (search-input-file (or native-inputs inputs)
                                             "/bin/bash"))))))))
    (native-inputs (list python-setuptools
                         python-wheel

                         python-pytest
                         python-pytest-cov
                         python-pytest-timeout
                         python-pytest-env
                         python-coverage
                         python-pytest-xdist
                         python-sphinx
                         python-pytest-doctestplus
                         which
                         python-pandas))
    (propagated-inputs (list python-acres
                             python-click
                             python-dateutil
                             python-etelemetry
                             python-filelock
                             python-looseversion
                             python-networkx
                             python-nibabel
                             python-numpy
                             python-packaging
                             python-prov
                             python-puremagic
                             python-pydot
                             (lookup-package-propagated-input python-prov "python-rdflib")
                             python-scipy
                             python-simplejson
                             python-traits))
    (home-page "https://nipype.readthedocs.io/en/latest/index.html")
    (synopsis "Neuroimaging in Python: Pipelines and Interfaces")
    (description "Nipype provides a uniform interface to existing neuroimaging software
and facilitates interaction between these packages within a single
workflow.  Nipype provides an environment that encourages interactive
exploration of algorithms from different packages.")
    (license license:asl2.0)))
