;;;
;;; Copyright © 2025 Alexis Simon <alexis.simon@runbox.com>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-science packages typst)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-tls)
  #:use-module (gnu packages crates-gtk)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-windows)
  #:use-module (gnu packages crates-crypto)
  #:use-module (gnu packages crates-web)
  #:use-module (gnu packages crates-compression)
  #:use-module (gnu packages crates-database)
  #:use-module (gnu packages crates-check)
  #:use-module (gnu packages tls)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system cargo))

(define-public typst
  (package
    (name "typst")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-cli" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "09zfrcc5awycl4906r5qhzfmf9qc2jxb30z0iq7dizqkd6wl900i"))))
    ;; Not yet supported by `guix refresh -t crate`, but differentiate with rust-typst
    (properties '((upstream-name . "typst-cli")))
    (build-system cargo-build-system)
    (arguments
     (list
      #:install-source? #f
      #:cargo-inputs `(("rust-chrono" ,rust-chrono-0.4)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-clap-complete" ,rust-clap-complete-4)
                       ("rust-clap-mangen" ,rust-clap-mangen-0.2)
                       ("rust-codespan-reporting" ,rust-codespan-reporting-0.11)
                       ("rust-color-print" ,rust-color-print-0.3)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-dirs" ,rust-dirs-6)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-fs-extra" ,rust-fs-extra-1)
                       ("rust-notify" ,rust-notify-8)
                       ("rust-open" ,rust-open-5)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-pathdiff" ,rust-pathdiff-0.2)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-same-file" ,rust-same-file-1)
                       ("rust-self-replace" ,rust-self-replace-1)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9)
                       ("rust-shell-escape" ,rust-shell-escape-0.1)
                       ("rust-sigpipe" ,rust-sigpipe-0.1)
                       ("rust-tar" ,rust-tar-0.4)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-tiny-http" ,rust-tiny-http-0.12)
                       ("rust-toml" ,rust-toml-0.8)
                       ("rust-typst" ,rust-typst-0.13)
                       ("rust-typst-eval" ,rust-typst-eval-0.13)
                       ("rust-typst-html" ,rust-typst-html-0.13)
                       ("rust-typst-kit" ,rust-typst-kit-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-pdf" ,rust-typst-pdf-0.13)
                       ("rust-typst-render" ,rust-typst-render-0.13)
                       ("rust-typst-svg" ,rust-typst-svg-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-ureq" ,rust-ureq-2)
                       ("rust-xz2" ,rust-xz2-0.1)
                       ("rust-zip" ,rust-zip-2))
      #:modules '((guix build cargo-build-system)
                  (guix build utils)
                  (ice-9 match))
      #:phases
      #~(modify-phases %standard-phases
          ;; TODO: (setenv "TYPST_VERSION" <short-git-commit-hash>-guix-checkout)
          (add-before 'build 'configure-artifacts
            (lambda _
              (mkdir-p "artifacts")
              (setenv "GEN_ARTIFACTS" "artifacts")))
          (add-after 'install 'install-man-page
            (lambda _
              (let ((man1 (string-append #$output "/share/man/man1")))
                (for-each (lambda (file)
                            (install-file file man1))
                          (find-files "artifacts/" "\\.1$")))))
          (add-after 'install 'install-shell-completions
            (lambda _
              (let* ((share (string-append #$output "/share"))
                     (shells
                      `(("bash-completion/typst"
                                      . ,(string-append share "/bash-completion/completions"))
                        ("_typst"     . ,(string-append share "/zsh/site-functions"))
                        ("typst.elv"  . ,(string-append share "/elvish/lib"))
                        ("typst.fish" . ,(string-append share "/fish/vendor_completions.d")))))
                (mkdir-p "artifacts/bash-completion")
                (rename-file "artifacts/typst.bash"
                             "artifacts/bash-completion/typst")
                (for-each (match-lambda
                           ((name . destination)
                            (install-file (string-append "artifacts/" name)
                                          destination)))
                          shells)))))))
    (propagated-inputs (list openssl))
    (native-search-paths
     (list (search-path-specification
            (variable "TYPST_PACKAGE_PATH")
            (files '("share/typst/packages"))
            (separator #f))
           (search-path-specification
            (variable "TYPST_FONT_PATHS")
            (files '("share/fonts" "share/texmf-dist/fonts")))))
    (home-page "https://typst.app/docs/")
    (synopsis "The command line interface for Typst")
    (description "Typst is a new markup-based typesetting system for the sciences. It is designed to be an alternative both to advanced tools like LaTeX and simpler tools like Word and Google Docs. Our goal with Typst is to build a typesetting tool that is highly capable and a pleasure to use.")
    (license license:asl2.0)))

(define-public rust-typst-0.13
  (package
    (name "rust-typst")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0qw0fzwmvxr6dvwi90hs26kw4jqiz3a0k066pmiv6k4qr7192xqz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-typst-eval" ,rust-typst-eval-0.13)
                       ("rust-typst-html" ,rust-typst-html-0.13)
                       ("rust-typst-layout" ,rust-typst-layout-0.13)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-realize" ,rust-typst-realize-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13))))
    (home-page "https://typst.app")
    (synopsis
     "new markup-based typesetting system that is powerful and easy to learn.")
    (description
     "This package provides a new markup-based typesetting system that is powerful and
easy to learn.")
    (license license:asl2.0)))

(define-public rust-typst-assets-0.13
  (package
    (name "rust-typst-assets")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-assets" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1dp04q2608fkvkv57vh38h8bisx05zk625kbdkm32kgppdmwal8h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://typst.app")
    (synopsis "Assets for the Typst compiler")
    (description "This package provides Assets for the Typst compiler.")
    (license license:asl2.0)))

(define-public rust-typst-eval-0.13
  (package
    (name "rust-typst-eval")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-eval" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0sy9gqdl1730iw3yc9p4dvmmlch0is37hvv3x81whvs643zr0xys"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-if-chain" ,rust-if-chain-1)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-stacker" ,rust-stacker-0.1)
                       ("rust-toml" ,rust-toml-0.8)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1))))
    (home-page "https://typst.app")
    (synopsis "Typst's code interpreter")
    (description "This package provides Typst's code interpreter.")
    (license license:asl2.0)))

(define-public rust-typst-html-0.13
  (package
    (name "rust-typst-html")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-html" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "16glm4wnrgd1cd410y4ljg4ig41kkvpwpv87wsm5n477daaxy3rv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-svg" ,rust-typst-svg-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13))))
    (home-page "https://typst.app")
    (synopsis "Typst's HTML exporter")
    (description "This package provides Typst's HTML exporter.")
    (license license:asl2.0)))

(define-public rust-typst-kit-0.13
  (package
    (name "rust-typst-kit")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-kit" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "05jyw7m1rv90g6b6l3zxpf1m2r78kadlnvwb3fmr0n5vyp3lg5xm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-dirs" ,rust-dirs-6)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-env-proxy" ,rust-env-proxy-0.4)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-fontdb" ,rust-fontdb-0.21)
                       ("rust-native-tls" ,rust-native-tls-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-openssl" ,rust-openssl-0.10)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-tar" ,rust-tar-0.4)
                       ("rust-typst" ,rust-typst-0.13)
                       ("rust-typst-assets" ,rust-typst-assets-0.13)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13)
                       ("rust-ureq" ,rust-ureq-2))))
    (home-page "https://typst.app")
    (synopsis "Common utilities for Typst tooling")
    (description "This package provides Common utilities for Typst tooling.")
    (license license:asl2.0)))

(define-public rust-typst-layout-0.13
  (package
    (name "rust-typst-layout")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-layout" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1z3pww0p217r0wzicr6bvw3lldrhidh1ak240l4iqijj2imz0pgx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-az" ,rust-az-1)
                       ("rust-bumpalo" ,rust-bumpalo-3)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-hypher" ,rust-hypher-0.1)
                       ("rust-icu-properties" ,rust-icu-properties-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-icu-provider-adapters" ,rust-icu-provider-adapters-1)
                       ("rust-icu-provider-blob" ,rust-icu-provider-blob-1)
                       ("rust-icu-segmenter" ,rust-icu-segmenter-1)
                       ("rust-kurbo" ,rust-kurbo-0.11)
                       ("rust-rustybuzz" ,rust-rustybuzz-0.18)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24)
                       ("rust-typst-assets" ,rust-typst-assets-0.13)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13)
                       ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
                       ("rust-unicode-math-class" ,rust-unicode-math-class-0.1)
                       ("rust-unicode-script" ,rust-unicode-script-0.5)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1))))
    (home-page "https://typst.app")
    (synopsis "Typst's layout engine")
    (description "This package provides Typst's layout engine.")
    (license license:asl2.0)))

(define-public rust-typst-library-0.13
  (package
    (name "rust-typst-library")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-library" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1f7hi5hs8r6kri2a1n83d526rv2pw4cs4paklv5y0brqg3df2v0n"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-az" ,rust-az-1)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-bumpalo" ,rust-bumpalo-3)
                       ("rust-chinese-number" ,rust-chinese-number-0.7)
                       ("rust-ciborium" ,rust-ciborium-0.2)
                       ("rust-codex" ,rust-codex-0.1)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-csv" ,rust-csv-1)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-fontdb" ,rust-fontdb-0.21)
                       ("rust-hayagriva" ,rust-hayagriva-0.8)
                       ("rust-icu-properties" ,rust-icu-properties-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-icu-provider-blob" ,rust-icu-provider-blob-1)
                       ("rust-image" ,rust-image-0.25)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-kamadak-exif" ,rust-kamadak-exif-0.6)
                       ("rust-kurbo" ,rust-kurbo-0.11)
                       ("rust-lipsum" ,rust-lipsum-0.9)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-palette" ,rust-palette-0.7)
                       ("rust-phf" ,rust-phf-0.11)
                       ("rust-png" ,rust-png-0.17)
                       ("rust-qcms" ,rust-qcms-0.3)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-regex-syntax" ,rust-regex-syntax-0.8)
                       ("rust-roxmltree" ,rust-roxmltree-0.20)
                       ("rust-rust-decimal" ,rust-rust-decimal-1)
                       ("rust-rustybuzz" ,rust-rustybuzz-0.18)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9)
                       ("rust-siphasher" ,rust-siphasher-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-syntect" ,rust-syntect-5)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-toml" ,rust-toml-0.8)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24)
                       ("rust-two-face" ,rust-two-face-0.4)
                       ("rust-typed-arena" ,rust-typed-arena-2)
                       ("rust-typst-assets" ,rust-typst-assets-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13)
                       ("rust-unicode-math-class" ,rust-unicode-math-class-0.1)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unscanny" ,rust-unscanny-0.1)
                       ("rust-usvg" ,rust-usvg-0.43)
                       ("rust-wasmi" ,rust-wasmi-0.40)
                       ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "https://typst.app")
    (synopsis "Typst's standard library")
    (description "This package provides Typst's standard library.")
    (license license:asl2.0)))

(define-public rust-typst-macros-0.13
  (package
    (name "rust-typst-macros")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0rcdxljjlgmgq3y6ylazrd3c1a3anycyiddq063ydi8wbrjv4j67"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-heck" ,rust-heck-0.5)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://typst.app")
    (synopsis "Proc-macros for Typst")
    (description "This package provides Proc-macros for Typst.")
    (license license:asl2.0)))

(define-public rust-typst-pdf-0.13
  (package
    (name "rust-typst-pdf")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-pdf" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1zwa31ydasky72r3gh8264nj5xkgvnqqg9ky6xjlajc6i68xlsbi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-image" ,rust-image-0.25)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-miniz-oxide" ,rust-miniz-oxide-0.8)
                       ("rust-pdf-writer" ,rust-pdf-writer-0.12)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-subsetter" ,rust-subsetter-0.2)
                       ("rust-svg2pdf" ,rust-svg2pdf-0.12)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24)
                       ("rust-typst-assets" ,rust-typst-assets-0.13)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13)
                       ("rust-xmp-writer" ,rust-xmp-writer-0.3))))
    (home-page "https://typst.app")
    (synopsis "PDF exporter for Typst")
    (description "This package provides PDF exporter for Typst.")
    (license license:asl2.0)))

(define-public rust-typst-realize-0.13
  (package
    (name "rust-typst-realize")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-realize" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1bri9dnqhgh9bgbmrqmmpf2q8y75avrlafc1za5z6bi1h0bzp20l"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-bumpalo" ,rust-bumpalo-3)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-utils" ,rust-typst-utils-0.13))))
    (home-page "https://typst.app")
    (synopsis "Typst's realization subsystem")
    (description "This package provides Typst's realization subsystem.")
    (license license:asl2.0)))

(define-public rust-typst-render-0.13
  (package
    (name "rust-typst-render")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-render" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1za9lbrmyp21sx80ja659ns5bix50dfc8f0j7zk6rc8lyr2anij9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-image" ,rust-image-0.25)
                       ("rust-pixglyph" ,rust-pixglyph-0.5)
                       ("rust-resvg" ,rust-resvg-0.43)
                       ("rust-tiny-skia" ,rust-tiny-skia-0.11)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13))))
    (home-page "https://typst.app")
    (synopsis "Raster image exporter for Typst")
    (description "This package provides Raster image exporter for Typst.")
    (license license:asl2.0)))

(define-public rust-typst-svg-0.13
  (package
    (name "rust-typst-svg")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-svg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1mlrgf83ydxbnsz185y1z1j80hkx2dy0jy2wyjv15ipf0xhqf1dq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64" ,rust-base64-0.22)
                       ("rust-comemo" ,rust-comemo-0.4)
                       ("rust-ecow" ,rust-ecow-0.2)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-image" ,rust-image-0.25)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24)
                       ("rust-typst-library" ,rust-typst-library-0.13)
                       ("rust-typst-macros" ,rust-typst-macros-0.13)
                       ("rust-typst-timing" ,rust-typst-timing-0.13)
                       ("rust-typst-timing" ,rust-typst-utils-0.13)
                       ("rust-xmlparser" ,rust-xmlparser-0.13)
                       ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "https://typst.app")
    (synopsis "SVG exporter for Typst")
    (description "This package provides SVG exporter for Typst.")
    (license license:asl2.0)))

(define-public rust-typst-syntax-0.13
  (package
    (name "rust-typst-syntax")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-syntax" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "049735g4ig5y05mx7p1d7wz075lqx6aq1wngbb9wm4n3j7hp1s5b"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ecow" ,rust-ecow-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-toml" ,rust-toml-0.8)
                       ("rust-typst-utils" ,rust-typst-utils-0.13)
                       ("rust-unicode-ident" ,rust-unicode-ident-1)
                       ("rust-unicode-math-class" ,rust-unicode-math-class-0.1)
                       ("rust-unicode-script" ,rust-unicode-script-0.5)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "https://typst.app")
    (synopsis "Parser and syntax tree for Typst")
    (description "This package provides Parser and syntax tree for Typst.")
    (license license:asl2.0)))

(define-public rust-typst-timing-0.13
  (package
    (name "rust-typst-timing")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-timing" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1wp4r8iq4nis4h0djp7i41h2q3qbd221jslhd93myz9xyy52hg8m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-typst-syntax" ,rust-typst-syntax-0.13))))
    (home-page "https://typst.app")
    (synopsis "Performance timing for Typst")
    (description "This package provides Performance timing for Typst.")
    (license license:asl2.0)))

(define-public rust-typst-utils-0.13
  (package
    (name "rust-typst-utils")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "typst-utils" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1aalyngas04mrn9sg57ycbpy919biy2brqpr4pvvjq0qzcrk70my"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-once-cell" ,rust-once-cell-1)
                       ("rust-portable-atomic" ,rust-portable-atomic-1)
                       ("rust-rayon" ,rust-rayon-1)
                       ("rust-siphasher" ,rust-siphasher-1)
                       ("rust-thin-vec" ,rust-thin-vec-0.2))))
    (home-page "https://typst.app")
    (synopsis "Utilities for Typst")
    (description "This package provides Utilities for Typst.")
    (license license:asl2.0)))

(define-public rust-hayagriva-0.8
  (package
    (name "rust-hayagriva")
    (version "0.8.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hayagriva" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1n7frakfqq5igpwwqwnqd4dw57i87vac45zrlhlvmz5p9dahfjcm"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-biblatex" ,rust-biblatex-0.10)
                       ("rust-ciborium" ,rust-ciborium-0.2)
                       ("rust-citationberg" ,rust-citationberg-0.5)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-numerals" ,rust-numerals-0.1)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9)
                       ("rust-strum" ,rust-strum-0.26)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-unic-langid" ,rust-unic-langid-0.9)
                       ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
                       ("rust-unscanny" ,rust-unscanny-0.1)
                       ("rust-url" ,rust-url-2))))
    (home-page "https://github.com/typst/hayagriva")
    (synopsis
     "Work with references: Literature database management, storage, and citation formatting")
    (description
     "This package provides Work with references: Literature database management, storage, and citation
formatting.")
    (license (list license:expat license:asl2.0))))

;===================
; Typst dependencies
(define-public rust-pixglyph-0.5
  (package
    (name "rust-pixglyph")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pixglyph" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "152lvambhwqhsg8c6imzaxc081hch4lcw12sdy3kvgrng29zlnni"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ttf-parser" ,rust-ttf-parser-0.24))))
    (home-page "https://github.com/typst/pixglyph")
    (synopsis "Font-rendering with subpixel positioning")
    (description
     "This package provides Font-rendering with subpixel positioning.")
    (license license:asl2.0)))

(define-public rust-xmp-writer-0.3
  (package
    (name "rust-xmp-writer")
    (version "0.3.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "xmp-writer" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "11x4nzryg2mbswqjn8bh7s7v1nnr1rv44gldx5lwip56ki69bdby"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/typst/xmp-writer")
    (synopsis "Write XMP metadata, step by step")
    (description "This package provides Write XMP metadata, step by step.")
    (license (list license:expat license:asl2.0))))

(define-public rust-image-webp-0.1
  (package
    (inherit rust-image-webp-0.2)
    (name "rust-image-webp")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "image-webp" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0179iqgvh6ybbq0r5d3ms11kka8jihhpliydkksj1vz2ps6gp6pp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))))

(define-public rust-resvg-0.43
  (package
    (name "rust-resvg")
    (version "0.43.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "resvg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1l8inhd3a1353851qkpsl0lbjdrc17fx6fp230ff6z4wqmilacf7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gif" ,rust-gif-0.13)
                       ("rust-image-webp" ,rust-image-webp-0.1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-pico-args" ,rust-pico-args-0.5)
                       ("rust-rgb" ,rust-rgb-0.8)
                       ("rust-svgtypes" ,rust-svgtypes-0.15)
                       ("rust-tiny-skia" ,rust-tiny-skia-0.11)
                       ("rust-usvg" ,rust-usvg-0.43)
                       ("rust-zune-jpeg" ,rust-zune-jpeg-0.4))))
    (home-page "https://github.com/RazrFalcon/resvg")
    (synopsis "An SVG rendering library")
    (description "This package provides An SVG rendering library.")
    (license license:mpl2.0)))

(define-public rust-svg2pdf-0.12
  (package
    (name "rust-svg2pdf")
    (version "0.12.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "svg2pdf" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0r03ql053ib4rjydaq2dw8ddxjdwbblkhr61z1zgn67kvkdcj52h"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-fontdb" ,rust-fontdb-0.21)
                       ("rust-image" ,rust-image-0.25)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-miniz-oxide" ,rust-miniz-oxide-0.8)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pdf-writer" ,rust-pdf-writer-0.12)
                       ("rust-resvg" ,rust-resvg-0.43)
                       ("rust-siphasher" ,rust-siphasher-1)
                       ("rust-subsetter" ,rust-subsetter-0.2)
                       ("rust-tiny-skia" ,rust-tiny-skia-0.11)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24)
                       ("rust-usvg" ,rust-usvg-0.43))))
    (home-page "https://github.com/typst/svg2pdf")
    (synopsis "Convert SVG files to PDFs")
    (description "This package provides Convert SVG files to PDFs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-subsetter-0.2
  (package
    (name "rust-subsetter")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "subsetter" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0j56xzd24imbd5520c69z96ylvbw00283mlkvvad8ms0ydw83ybl"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/typst/subsetter")
    (synopsis "Reduces the size and coverage of OpenType fonts")
    (description
     "This package provides Reduces the size and coverage of @code{OpenType} fonts.")
    (license (list license:expat license:asl2.0))))

(define-public rust-pdf-writer-0.12
  (package
    (name "rust-pdf-writer")
    (version "0.12.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pdf-writer" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1mdi4s5f164dbhczi5mvjz2z4q2s73qhdvwqyf9nzq3d45ykrw2x"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-itoa" ,rust-itoa-1)
                       ("rust-memchr" ,rust-memchr-2)
                       ("rust-ryu" ,rust-ryu-1))))
    (home-page "https://github.com/typst/pdf-writer")
    (synopsis "step-by-step PDF writer.")
    (description "This package provides a step-by-step PDF writer.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wasmi-core-0.40
  (package
    (name "rust-wasmi-core")
    (version "0.40.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmi_core" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0cvw16b7l3his0p6m8bv0nywawyxpv91q8gzqz132bf35i45331s"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-downcast-rs" ,rust-downcast-rs-1)
                       ("rust-libm" ,rust-libm-0.2))))
    (home-page "https://github.com/wasmi-labs/wasmi")
    (synopsis "Core primitives for the wasmi WebAssembly interpreter")
    (description
     "This package provides Core primitives for the wasmi @code{WebAssembly} interpreter.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wasmi-collections-0.40
  (package
    (name "rust-wasmi-collections")
    (version "0.40.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmi_collections" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0qimh7lb0pdicdlirdp2794i6rippxs1amlxjchj14hwbcknn3g8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-hashbrown" ,rust-hashbrown-0.15)
                       ("rust-string-interner" ,rust-string-interner-0.18))))
    (home-page "https://github.com/wasmi-labs/wasmi")
    (synopsis "Specialized data structures for the Wasmi interpreter")
    (description
     "This package provides Specialized data structures for the Wasmi interpreter.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unicode-vo-0.1
  (package
    (name "rust-unicode-vo")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-vo" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "151sha088v9jyfvbg5164xh4dk72g53b82xm4zzbf5dlagzqdlxi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/RazrFalcon/unicode-vo")
    (synopsis "Unicode vertical orientation detection")
    (description
     "This package provides Unicode vertical orientation detection.")
    (license (list license:expat license:asl2.0))))

(define-public rust-svgtypes-0.15
  (package
    (name "rust-svgtypes")
    (version "0.15.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "svgtypes" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "03h0raj2lpd6hdc47r37q0w3w2zlcm5418xn1bnvxalfqhyfakbr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-kurbo" ,rust-kurbo-0.11)
                       ("rust-siphasher" ,rust-siphasher-1))))
    (home-page "https://github.com/RazrFalcon/svgtypes")
    (synopsis "SVG types parser")
    (description "This package provides SVG types parser.")
    (license (list license:expat license:asl2.0))))

(define-public rust-strict-num-0.1
  (package
    (name "rust-strict-num")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "strict-num" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0cb7l1vhb8zj90mzm8avlk815k40sql9515s865rqdrdfavvldv6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-float-cmp" ,rust-float-cmp-0.9))))
    (home-page "https://github.com/RazrFalcon/strict-num")
    (synopsis "collection of bounded numeric types")
    (description
     "This package provides a collection of bounded numeric types.")
    (license license:expat)))

(define-public rust-simplecss-0.2
  (package
    (name "rust-simplecss")
    (version "0.2.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "simplecss" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0v0kid7b2602kcka2x2xs9wwfjf8lnvpgpl8x287qg4wra1ni73s"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-log" ,rust-log-0.4))))
    (home-page "https://github.com/linebender/simplecss")
    (synopsis "simple CSS 2 parser and selector.")
    (description "This package provides a simple CSS 2 parser and selector.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-imagesize-0.13
  (package
    (name "rust-imagesize")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "imagesize" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "11f26ac9zvbr7sjnsv2z9jd3ryaz40pg8xch4ij1q1rg5zbjgkgd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/Roughsketch/imagesize")
    (synopsis
     "Quick probing of image dimensions without loading the entire file")
    (description
     "This package provides Quick probing of image dimensions without loading the entire file.")
    (license license:expat)))

(define-public rust-usvg-0.43
  (package
    (name "rust-usvg")
    (version "0.43.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "usvg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1z9mfx1nw00kpjz3rfh5qz91vjmlm7ric8nfp2gnwhmvbixha0v8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-base64" ,rust-base64-0.22)
                       ("rust-data-url" ,rust-data-url-0.3)
                       ("rust-flate2" ,rust-flate2-1)
                       ("rust-fontdb" ,rust-fontdb-0.21)
                       ("rust-imagesize" ,rust-imagesize-0.13)
                       ("rust-kurbo" ,rust-kurbo-0.11)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-pico-args" ,rust-pico-args-0.5)
                       ("rust-roxmltree" ,rust-roxmltree-0.20)
                       ("rust-rustybuzz" ,rust-rustybuzz-0.18)
                       ("rust-simplecss" ,rust-simplecss-0.2)
                       ("rust-siphasher" ,rust-siphasher-1)
                       ("rust-strict-num" ,rust-strict-num-0.1)
                       ("rust-svgtypes" ,rust-svgtypes-0.15)
                       ("rust-tiny-skia-path" ,rust-tiny-skia-path-0.11)
                       ("rust-unicode-bidi" ,rust-unicode-bidi-0.3)
                       ("rust-unicode-script" ,rust-unicode-script-0.5)
                       ("rust-unicode-vo" ,rust-unicode-vo-0.1)
                       ("rust-xmlwriter" ,rust-xmlwriter-0.1))))
    (home-page "https://github.com/RazrFalcon/resvg")
    (synopsis "An SVG simplification library")
    (description "This package provides An SVG simplification library.")
    (license license:mpl2.0)))

(define-public rust-unicode-math-class-0.1
  (package
    (name "rust-unicode-math-class")
    (version "0.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-math-class" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0rbxcjirldpdrpxv1l7qiadbib8rnl7b413fsp4f7ynmk7snq93x"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/typst/unicode-math-class")
    (synopsis "Determine the Unicode class of a mathematical character")
    (description
     "This package provides Determine the Unicode class of a mathematical character.")
    (license (list license:expat license:asl2.0))))

(define-public rust-indexmap-nostd-0.4
  (package
    (name "rust-indexmap-nostd")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "indexmap-nostd" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "145mrkrrnzzg8xbv6si8j3b8cw1pi3g13vrjgf1fm2415gyy414f"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/robbepop/indexmap-nostd")
    (synopsis "no_std compatible implementation of the indexmap crate")
    (description
     "This package provides a no_std compatible implementation of the indexmap crate.")
    (license license:asl2.0)))

(define-public rust-wasmparser-nostd-0.100
  (package
    (name "rust-wasmparser-nostd")
    (version "0.100.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmparser-nostd" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ak4bi9k9jb223xw7mlxkgim6lp7m8bwfqhlpfa4ll7kjpz1b86m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-indexmap-nostd" ,rust-indexmap-nostd-0.4))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wasmparser")
    (synopsis
     "simple event-driven library for parsing WebAssembly binary files.")
    (description
     "This package provides a simple event-driven library for parsing
@code{WebAssembly} binary files.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-string-interner-0.18
  (package
    (name "rust-string-interner")
    (version "0.18.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "string-interner" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "16vg2fphp673h9pqsk46lbdj91baz4p9rj2pmi62v7vs9m37achs"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-hashbrown" ,rust-hashbrown-0.15)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/robbepop/string-interner")
    (synopsis "Efficient string interner with minimal memory footprint
and fast access to the underlying strings.")
    (description
     "This package provides Efficient string interner with minimal memory footprint and fast access to the
underlying strings.")
    (license (list license:expat license:asl2.0))))

(define-public rust-multi-stash-0.2
  (package
    (name "rust-multi-stash")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "multi-stash" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "03s12sf633n02mcqcv2yxdx545lwc127hsic3n774khznv29lnk8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/robbepop/multi-stash")
    (synopsis "Vector-based arena data structure that reuses vacant slots")
    (description
     "This package provides Vector-based arena data structure that reuses vacant slots.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wasmi-0.40
  (package
    (name "rust-wasmi")
    (version "0.40.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmi" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0qq069di8lpb2vqckifv4cbyifxx98mkxlmlsv8ms14nrdzzk6m1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-multi-stash" ,rust-multi-stash-0.2)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-spin" ,rust-spin-0.9)
                       ("rust-wasmi-collections" ,rust-wasmi-collections-0.40)
                       ("rust-wasmi-core" ,rust-wasmi-core-0.40)
                       ("rust-wasmi-ir" ,rust-wasmi-ir-0.40)
                       ("rust-wasmparser" ,rust-wasmparser-0.221))))
    (home-page "https://github.com/wasmi-labs/wasmi")
    (synopsis "WebAssembly interpreter")
    (description "This package provides @code{WebAssembly} interpreter.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wasmi-ir-0.40
  (package
    (name "rust-wasmi-ir")
    (version "0.40.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmi_ir" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "16i8nmkjhlj2ywfqvq51gbw53vb8pn46flc858hmknw6q4a1lhvf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-wasmi-core" ,rust-wasmi-core-0.40))))
    (home-page "https://github.com/wasmi-labs/wasmi")
    (synopsis "WebAssembly interpreter internal bytecode representation")
    (description
     "This package provides @code{WebAssembly} interpreter internal bytecode representation.")
    (license (list license:expat license:asl2.0))))

(define-public rust-wasmparser-0.221
  (package
    (name "rust-wasmparser")
    (version "0.221.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "wasmparser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "11ficyz79dcypkxxg1c8vl8bm0avg8a80csnxq6vxhismcvglsyh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-hashbrown" ,rust-hashbrown-0.15)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page
     "https://github.com/bytecodealliance/wasm-tools/tree/main/crates/wasmparser")
    (synopsis
     "simple event-driven library for parsing WebAssembly binary files.")
    (description
     "This package provides a simple event-driven library for parsing
@code{WebAssembly} binary files.")
    (license
      (list license:asl2.0
            (license:non-copyleft
              "https://github.com/bytecodealliance/wasm-tools/blob/main/LICENSE-Apache-2.0_WITH_LLVM-exception"
              "Apache 2 Licence with LLVM exceptions")
            license:expat))))

(define-public rust-unicode-script-0.5
  (package
    (name "rust-unicode-script")
    (version "0.5.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-script" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "07vwr9iddw5xwrj57hc6ig0mwmlzjdajj9lyfxqz9by9a2rj3d4z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
                       ("rust-rustc-std-workspace-std" ,rust-rustc-std-workspace-std-1))))
    (home-page "https://github.com/unicode-rs/unicode-script")
    (synopsis
     "This crate exposes the Unicode `Script` and `Script_Extension` properties from [UAX #24](http://www.unicode.org/reports/tr24/)")
    (description
     "This crate exposes the Unicode `Script` and `Script_Extension` properties from
[UAX #24](http://www.unicode.org/reports/tr24/).")
    (license (list license:expat license:asl2.0))))

(define-public rust-unicode-properties-0.1
  (package
    (name "rust-unicode-properties")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-properties" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1l3mbgzwz8g14xcs09p4ww3hjkjcf0i1ih13nsg72bhj8n5jl3z7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/unicode-rs/unicode-properties")
    (synopsis "Query character Unicode properties according to
UAX #44 and UTR #51.")
    (description
     "This package provides Query character Unicode properties according to UAX #44 and UTR #51.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unicode-ccc-0.3
  (package
    (name "rust-unicode-ccc")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-ccc" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0hnyjqjaxxnsqfhc7capa9dbi5jzl41hhdk80slsk4rqgdjcc2r6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/RazrFalcon/unicode-ccc")
    (synopsis "Unicode Canonical Combining Class detection")
    (description
     "This package provides Unicode Canonical Combining Class detection.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unicode-bidi-mirroring-0.3
  (package
    (name "rust-unicode-bidi-mirroring")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-bidi-mirroring" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0bq832hqj8qbk14sk4qxyi3zk2ldhdhvw9ii2759ar26sxx0bbv4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/RazrFalcon/unicode-bidi-mirroring")
    (synopsis "Unicode Bidi Mirroring property detection")
    (description
     "This package provides Unicode Bidi Mirroring property detection.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rustybuzz-0.18
  (package
    (name "rust-rustybuzz")
    (version "0.18.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rustybuzz" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "108igxavhzmln7h6qgfxk3bdghp8hvlc8lpbk13q6qcya76iqpf8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-core-maths" ,rust-core-maths-0.1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24)
                       ("rust-unicode-bidi-mirroring" ,rust-unicode-bidi-mirroring-0.3)
                       ("rust-unicode-ccc" ,rust-unicode-ccc-0.3)
                       ("rust-unicode-properties" ,rust-unicode-properties-0.1)
                       ("rust-unicode-script" ,rust-unicode-script-0.5)
                       ("rust-wasmi" ,rust-wasmi-0.40))))
    (home-page "https://github.com/harfbuzz/rustybuzz")
    (synopsis "complete harfbuzz shaping algorithm port to Rust.")
    (description
     "This package provides a complete harfbuzz shaping algorithm port to Rust.")
    (license license:expat)))

(define-public rust-pq-src-0.3
  (package
    (name "rust-pq-src")
    (version "0.3.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pq-src" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1is8344ffcpv7raym9h4n0dz06sxyr9n4vhcrb850ghxpx7z9yr8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1)
                       ("rust-openssl-sys" ,rust-openssl-sys-0.9))))
    (home-page "https://github.com/sgrif/pq-sys")
    (synopsis "Bundled version of libpq")
    (description "This package provides Bundled version of libpq.")
    (license (list license:expat license:asl2.0))))

(define-public rust-openssl-src-300
  (package
    (name "rust-openssl-src")
    (version "300.4.1+3.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "openssl-src" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1337svym5imvq9ww04xh0ss38krhbhfb7l92ar5l2qlc2g2fm97s"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cc" ,rust-cc-1))))
    (home-page "https://github.com/alexcrichton/openssl-src-rs")
    (synopsis "Source of OpenSSL and logic to build it.")
    (description
     "This package provides Source of @code{OpenSSL} and logic to build it.")
    (license (list license:expat license:asl2.0))))

(define-public rust-mysqlclient-src-0.1
  (package
    (name "rust-mysqlclient-src")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "mysqlclient-src" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "15f64s43af0z6608mja7q7wzxiakbsxgad2f4ffpw89g8ppfxjac"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cmake" ,rust-cmake-0.1)
                       ("rust-link-cplusplus" ,rust-link-cplusplus-1)
                       ("rust-openssl-src" ,rust-openssl-src-300)
                       ("rust-openssl-sys" ,rust-openssl-sys-0.9))))
    (home-page "https://github.com/sgrif/mysqlclient-sys")
    (synopsis "Bundled version of libmysqlclient")
    (description "This package provides Bundled version of libmysqlclient.")
    (license license:gpl2)))

(define-public rust-dsl-auto-type-0.1
  (package
    (name "rust-dsl-auto-type")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "dsl_auto_type" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "01xng43pn2dlc5k422is20dapq14w9x1p46qq968c0s167kapnf5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-darling" ,rust-darling-0.20)
                       ("rust-either" ,rust-either-1)
                       ("rust-heck" ,rust-heck-0.5)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://diesel.rs")
    (synopsis
     "Automatically expand query fragment types for factoring as functions")
    (description
     "This package provides Automatically expand query fragment types for factoring as functions.")
    (license (list license:expat license:asl2.0))))

(define-public rust-diesel-table-macro-syntax-0.2
  (package
    (name "rust-diesel-table-macro-syntax")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "diesel_table_macro_syntax" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "09gvkyljhchbxfkxlkkrdcqcmcxwsim9sfljqilbq4x485b77710"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-syn" ,rust-syn-2))))
    (home-page "https://diesel.rs")
    (synopsis "Internal diesel crate")
    (description "This package provides Internal diesel crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-diesel-derives-2
  (package
    (name "rust-diesel-derives")
    (version "2.2.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "diesel_derives" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "191iw5ja7s1gjy9ymjvv91ghzbvs2fb5ca28lvr6pfp2a7gc7wp7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-diesel-table-macro-syntax" ,rust-diesel-table-macro-syntax-0.2)
                       ("rust-dsl-auto-type" ,rust-dsl-auto-type-0.1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://diesel.rs")
    (synopsis
     "You should not use this crate directly, it is internal to Diesel")
    (description
     "This package provides You should not use this crate directly, it is internal to Diesel.")
    (license (list license:expat license:asl2.0))))

(define-public rust-diesel-2
  (package
    (name "rust-diesel")
    (version "2.2.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "diesel" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "04kxz9gss7wzis30bcgplxx8xkm635dx2vd30hr69ffdckgvxwfc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bigdecimal" ,rust-bigdecimal-0.1)
                       ("rust-bitflags" ,rust-bitflags-2)
                       ("rust-byteorder" ,rust-byteorder-1)
                       ("rust-chrono" ,rust-chrono-0.4)
                       ("rust-diesel-derives" ,rust-diesel-derives-2)
                       ("rust-ipnet" ,rust-ipnet-2)
                       ("rust-ipnetwork" ,rust-ipnetwork-0.17)
                       ("rust-itoa" ,rust-itoa-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-libsqlite3-sys" ,rust-libsqlite3-sys-0.20)
                       ("rust-mysqlclient-src" ,rust-mysqlclient-src-0.1)
                       ("rust-mysqlclient-sys" ,rust-mysqlclient-sys-0.2)
                       ("rust-num-bigint" ,rust-num-bigint-0.2)
                       ("rust-num-integer" ,rust-num-integer-0.1)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-percent-encoding" ,rust-percent-encoding-2)
                       ("rust-pq-src" ,rust-pq-src-0.3)
                       ("rust-pq-sys" ,rust-pq-sys-0.4)
                       ("rust-quickcheck" ,rust-quickcheck-1)
                       ("rust-r2d2" ,rust-r2d2-0.8)
                       ("rust-serde-json" ,rust-serde-json-0.9)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-url" ,rust-url-2)
                       ("rust-uuid" ,rust-uuid-0.7))))
    (home-page "https://diesel.rs")
    (synopsis
     "safe, extensible ORM and Query Builder for PostgreSQL, SQLite, and MySQL")
    (description
     "This package provides a safe, extensible ORM and Query Builder for
@code{PostgreSQL}, SQLite, and @code{MySQL}.")
    (license (list license:expat license:asl2.0))))

(define-public rust-rust-decimal-1
  (package
    (name "rust-rust-decimal")
    (version "1.36.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "rust_decimal" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0mgmplkpawx9kggc4v3qymmdxx71dx1qsf1lsqp2pi9w7q7di0mh"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-borsh" ,rust-borsh-1)
                       ("rust-bytes" ,rust-bytes-1)
                       ("rust-diesel" ,rust-diesel-2)
                       ("rust-ndarray" ,rust-ndarray-0.15)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-postgres-types" ,rust-postgres-types-0.2)
                       ("rust-proptest" ,rust-proptest-1)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-rocket" ,rust-rocket-0.5)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-tokio-postgres" ,rust-tokio-postgres-0.7))))
    (home-page "https://github.com/paupino/rust-decimal")
    (synopsis
     "Decimal number implementation written in pure Rust suitable for financial and fixed-precision calculations")
    (description
     "This package provides Decimal number implementation written in pure Rust suitable for financial and
fixed-precision calculations.")
    (license license:expat)))

(define-public rust-qcms-0.3
  (package
    (name "rust-qcms")
    (version "0.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "qcms" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1yihv9rsa0qc4mmhzp8f0xdfrnkw7q8l7kr4ivcyb9amszazrv7d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/FirefoxGraphics/qcms")
    (synopsis "lightweight color management")
    (description "This package provides lightweight color management.")
    (license license:expat)))

(define-public rust-lipsum-0.9
  (package
    (name "rust-lipsum")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "lipsum" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0r40mf2cwh4fp9pdfcc1n8hjxw05w7galjvb1z23r5pq38jn0s33"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-rand" ,rust-rand-0.8)
                       ("rust-rand-chacha" ,rust-rand-chacha-0.3))))
    (home-page "https://github.com/mgeisler/lipsum/")
    (synopsis
     "Lipsum is a lorem ipsum text generation library. It generates
pseudo-random Latin text. Use this if you need filler or dummy text
for your application.

The text is generated using a simple Markov chain, which you can
instantiate to generate your own pieces of pseudo-random text.")
    (description
     "This package provides Lipsum is a lorem ipsum text generation library.  It generates pseudo-random
Latin text.  Use this if you need filler or dummy text for your application.
The text is generated using a simple Markov chain, which you can instantiate to
generate your own pieces of pseudo-random text.")
    (license license:expat)))

(define-public rust-kurbo-0.11
  (package
    (name "rust-kurbo")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "kurbo" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "13sij8pmjp8rn8gwzx5fzx623m55s4fv9rmxgs9dv9qhqqn4n8w9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arrayvec" ,rust-arrayvec-0.7)
                       ("rust-libm" ,rust-libm-0.2)
                       ("rust-mint" ,rust-mint-0.5)
                       ("rust-schemars" ,rust-schemars-0.8)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-smallvec" ,rust-smallvec-1))))
    (home-page "https://github.com/linebender/kurbo")
    (synopsis "2D curves library")
    (description "This package provides a 2D curves library.")
    (license (list license:expat license:asl2.0))))

(define-public rust-mutate-once-0.1
  (package
    (name "rust-mutate-once")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "mutate_once" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ys9mpjhwsj5md10ykmkin0wv7bz8dvc292hqczs9l5l4cd6ikqn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/kamadak/mutate_once-rs")
    (synopsis "Interior mutability, write-once and borrowable as plain &T")
    (description
     "This package provides Interior mutability, write-once and borrowable as plain &T.")
    (license license:bsd-2)))

(define-public rust-kamadak-exif-0.6
  (package
    (name "rust-kamadak-exif")
    (version "0.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "kamadak-exif" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ds8k36qi88mzhx5fm1cgbxg0s4kmyiibmqpl5asvvvlfc6dhc0i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-mutate-once" ,rust-mutate-once-0.1))))
    (home-page "https://github.com/kamadak/exif-rs")
    (synopsis "Exif parsing library written in pure Rust")
    (description
     "This package provides Exif parsing library written in pure Rust.")
    (license license:bsd-2)))

(define-public rust-icu-segmenter-data-1
  (package
    (name "rust-icu-segmenter-data")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_segmenter_data" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1pvrgnxi7fq47hfpc66jgvxzfc8nmzmgv0xw63imbnb0f9rywfgp"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Data for the icu_segmenter crate")
    (description "This package provides Data for the icu_segmenter crate.")
    (license license:unicode)))

(define-public rust-icu-segmenter-1
  (package
    (name "rust-icu-segmenter")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_segmenter" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1pmharib9s1hn5650d4lyd48145n1n14pja2gcnzqvrl29b745x7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-core-maths" ,rust-core-maths-0.1)
                       ("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-icu-collections" ,rust-icu-collections-1)
                       ("rust-icu-locid" ,rust-icu-locid-1)
                       ("rust-icu-locid-transform" ,rust-icu-locid-transform-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-icu-segmenter-data" ,rust-icu-segmenter-data-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-utf8-iter" ,rust-utf8-iter-1)
                       ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis
     "Unicode line breaking and text segmentation algorithms for text boundaries analysis")
    (description
     "This package provides Unicode line breaking and text segmentation algorithms for text boundaries
analysis.")
    (license license:unicode)))

(define-public rust-zerotrie-0.1
  (package
    (name "rust-zerotrie")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "zerotrie" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "07qa5ljss8j706iy0rd023naamwly4jfwz0pc1gmqcw7bpalsngv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-displaydoc" ,rust-displaydoc-0.2)
                       ("rust-litemap" ,rust-litemap-0.7)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-yoke" ,rust-yoke-0.7)
                       ("rust-zerofrom" ,rust-zerofrom-0.1)
                       ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "data structure that efficiently maps strings to integers")
    (description
     "This package provides a data structure that efficiently maps strings to
integers.")
    (license license:unicode)))

(define-public rust-icu-provider-blob-1
  (package
    (name "rust-icu-provider-blob")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_provider_blob" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0lfgvia5v76gkpfgbga5ga6z1b5465v821f2hs0xfmaz6v8rhjy2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-postcard" ,rust-postcard-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-writeable" ,rust-writeable-0.5)
                       ("rust-zerotrie" ,rust-zerotrie-0.1)
                       ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "ICU4X data provider that reads from blobs in memory")
    (description
     "This package provides ICU4X data provider that reads from blobs in memory.")
    (license license:unicode)))

(define-public rust-icu-provider-adapters-1
  (package
    (name "rust-icu-provider-adapters")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "icu_provider_adapters" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1g60fydak0i4rxf8vfrr31mpck846k9ynix4fh1qx2il13ylscnn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-databake" ,rust-databake-0.1)
                       ("rust-icu-locid" ,rust-icu-locid-1)
                       ("rust-icu-locid-transform" ,rust-icu-locid-transform-1)
                       ("rust-icu-provider" ,rust-icu-provider-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-zerovec" ,rust-zerovec-0.10))))
    (home-page "https://icu4x.unicode.org")
    (synopsis "Adapters for composing and manipulating data providers")
    (description
     "This package provides Adapters for composing and manipulating data providers.")
    (license license:unicode)))

(define-public rust-hypher-0.1
  (package
    (name "rust-hypher")
    (version "0.1.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "hypher" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ra2kxbpi033jy42wkr7m7rgg6yhy69xad0hmc0z43936xbas91v"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/typst/hypher")
    (synopsis "hypher separates words into syllables")
    (description
     "This package provides hypher separates words into syllables.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-macros-impl-0.9
  (package
    (name "rust-unic-langid-macros-impl")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid-macros-impl" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0nsm0hky2sawgkwz511br06mkm3ba70rfc05jm0l54x3gciz9mqy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2)
                       ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-macros-0.9
  (package
    (name "rust-unic-langid-macros")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0pi71r5474n7sdmyky7qpnia9rrr42q0d200l5lpag1d0hncv88d"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro-hack" ,rust-proc-macro-hack-0.5)
                       ("rust-tinystr" ,rust-tinystr-0.7)
                       ("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
                       ("rust-unic-langid-macros-impl" ,rust-unic-langid-macros-impl-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-impl-0.9
  (package
    (name "rust-unic-langid-impl")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid-impl" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1rckyn5wqd5h8jxhbzlbbagr459zkzg822r4k5n30jaryv0j4m0a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-tinystr" ,rust-tinystr-0.7))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-unic-langid-0.9
  (package
    (name "rust-unic-langid")
    (version "0.9.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unic-langid" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0i2s024frmpfa68lzy8y8vnb1rz3m9v0ga13f7h2afx7f8g9vp93"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-unic-langid-impl" ,rust-unic-langid-impl-0.9)
                       ("rust-unic-langid-macros" ,rust-unic-langid-macros-0.9))))
    (home-page "https://github.com/zbraniecki/unic-locale")
    (synopsis "API for managing Unicode Language Identifiers")
    (description
     "This package provides API for managing Unicode Language Identifiers.")
    (license (list license:expat license:asl2.0))))

(define-public rust-citationberg-0.5
  (package
    (name "rust-citationberg")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "citationberg" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1yj4z1iivzw5jmcwlb32zig54qindllbb0000wsh5d5gpq1mwng4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-quick-xml" ,rust-quick-xml-0.36)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "https://github.com/typst/citationberg")
    (synopsis "parser for CSL files")
    (description "This package provides a parser for CSL files.")
    (license (list license:expat license:asl2.0))))

(define-public rust-numerals-0.1
  (package
    (name "rust-numerals")
    (version "0.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "numerals" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0cdx6yf5zcx2nvmzavr4qk9m35ha6i2rhy5fjxgx2wm7fq9y4nz2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/ogham/rust-numerals")
    (synopsis "Library for numeric systems, both ancient and modern")
    (description
     "This package provides Library for numeric systems, both ancient and modern.")
    (license license:expat)))

(define-public rust-biblatex-0.10
  (package
    (name "rust-biblatex")
    (version "0.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "biblatex" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1hpkxwkyby2bx8gn6jnfryn3da8ihxjhmpfhc15zkgmxzhbp6nm3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-numerals" ,rust-numerals-0.1)
                       ("rust-paste" ,rust-paste-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-strum" ,rust-strum-0.26)
                       ("rust-unicode-normalization" ,rust-unicode-normalization-0.1)
                       ("rust-unscanny" ,rust-unscanny-0.1))))
    (home-page "https://github.com/typst/biblatex")
    (synopsis "Parsing, writing, and evaluating BibTeX and BibLaTeX files")
    (description
     "This package provides Parsing, writing, and evaluating @code{BibTeX} and @code{BibLaTeX} files.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ttf-parser-0.24
  (package
    (name "rust-ttf-parser")
    (version "0.24.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ttf-parser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0jmcif537g3smzgcx0vfr4wwq4pnaypa7dnklasfhf2xzy813qjv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-core-maths" ,rust-core-maths-0.1))))
    (home-page "https://github.com/harfbuzz/ttf-parser")
    (synopsis
     "high-level, safe, zero-allocation font parser for TrueType, OpenType, and AAT.")
    (description
     "This package provides a high-level, safe, zero-allocation font parser for
@code{TrueType}, @code{OpenType}, and AAT.")
    (license (list license:expat license:asl2.0))))

(define-public rust-fontconfig-parser-0.5
  (package
    (name "rust-fontconfig-parser")
    (version "0.5.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "fontconfig-parser" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "19xdfsvl9sjha9n1lk3s6bqixcihsmjsd7zf3y90rsd69kagrz61"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-log" ,rust-log-0.4)
                       ("rust-roxmltree" ,rust-roxmltree-0.20)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/Riey/fontconfig-parser")
    (synopsis "fontconfig file parser in pure Rust")
    (description "This package provides fontconfig file parser in pure Rust.")
    (license license:expat)))

(define-public rust-fontdb-0.21
  (package
    (name "rust-fontdb")
    (version "0.21.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "fontdb" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ywa563zfmq501pfhp7qn1zlfj9kyxkmg92prlwf8swn1p19zgip"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-fontconfig-parser" ,rust-fontconfig-parser-0.5)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-memmap2" ,rust-memmap2-0.9)
                       ("rust-slotmap" ,rust-slotmap-1)
                       ("rust-tinyvec" ,rust-tinyvec-1)
                       ("rust-ttf-parser" ,rust-ttf-parser-0.24))))
    (home-page "https://github.com/RazrFalcon/fontdb")
    (synopsis "simple, in-memory font database with CSS-like queries.")
    (description
     "This package provides a simple, in-memory font database with CSS-like queries.")
    (license license:expat)))

(define-public rust-chinese-variant-1
  (package
    (name "rust-chinese-variant")
    (version "1.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "chinese-variant" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12s91vg2m9wfs9b3f0q2alj9am08y7r2prb0szg3fwjh8m8lg23m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-enum-ordinalize" ,rust-enum-ordinalize-4))))
    (home-page "https://magiclen.org/chinese-variant")
    (synopsis "An enum to represent the variants of the Chinese Language")
    (description
     "This package provides An enum to represent the variants of the Chinese Language.")
    (license license:expat)))

(define-public rust-chinese-number-0.7
  (package
    (name "rust-chinese-number")
    (version "0.7.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "chinese-number" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0v5va8drix8gs2kv6pmv5yzdxhlpzrwkp3ch86kxdxj6cgpwmz29"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-chinese-variant" ,rust-chinese-variant-1)
                       ("rust-enum-ordinalize" ,rust-enum-ordinalize-4)
                       ("rust-num-bigint" ,rust-num-bigint-0.4)
                       ("rust-num-traits" ,rust-num-traits-0.2))))
    (home-page "https://magiclen.org/chinese-number")
    (synopsis
     "Convert primitive numbers to Chinese numbers, or parse Chinese numbers to primitive numbers")
    (description
     "This package provides Convert primitive numbers to Chinese numbers, or parse Chinese numbers to
primitive numbers.")
    (license license:expat)))

(define-public rust-self-replace-1
  (package
    (name "rust-self-replace")
    (version "1.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "self-replace" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1drganasvf5b0x6c9g60jkfhzjc9in3r6cznjfw0lhmbbrdq3v03"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-fastrand" ,rust-fastrand-2)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-windows-sys" ,rust-windows-sys-0.52))))
    (home-page "https://github.com/mitsuhiko/self-replace")
    (synopsis
     "Utility crate that allows executables to replace or uninstall themselves")
    (description
     "This package provides Utility crate that allows executables to replace or uninstall themselves.")
    (license license:asl2.0)))

(define-public rust-ecow-0.2
  (package
    (name "rust-ecow")
    (version "0.2.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ecow" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1fhrh7lsx82bqdsl57p4zdds4d8nmwgdcncyp5c0rclj76lw0bz4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-loom" ,rust-loom-0.7)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/typst/ecow")
    (synopsis "Compact, clone-on-write vector and string")
    (description
     "This package provides Compact, clone-on-write vector and string.")
    (license (list license:expat license:asl2.0))))

(define-public rust-siphasher-1
  (package
    (name "rust-siphasher")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "siphasher" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "17f35782ma3fn6sh21c027kjmd227xyrx06ffi8gw4xzv9yry6an"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://docs.rs/siphasher")
    (synopsis "SipHash-2-4, SipHash-1-3 and 128-bit variants in pure Rust")
    (description
     "This package provides @code{SipHash-2-4}, @code{SipHash-1-3} and 128-bit variants in pure Rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-comemo-macros-0.4
  (package
    (name "rust-comemo-macros")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "comemo-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1nr8w81hkzg49s515v61shxb077iq6d6001pybxbvxdlz516x4y8"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/typst/comemo")
    (synopsis "Procedural macros for comemo")
    (description "This package provides Procedural macros for comemo.")
    (license (list license:expat license:asl2.0))))

(define-public rust-comemo-0.4
  (package
    (name "rust-comemo")
    (version "0.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "comemo" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "14bsiayib4lhz3jrbf1fqh2fpwsm6cii90mifym3jhvji901csfz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-comemo-macros" ,rust-comemo-macros-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-siphasher" ,rust-siphasher-1))))
    (home-page "https://github.com/typst/comemo")
    (synopsis "Incremental computation through constrained memoization")
    (description
     "This package provides Incremental computation through constrained memoization.")
    (license (list license:expat license:asl2.0))))

(define-public rust-sigpipe-0.1
  (package
    (name "rust-sigpipe")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "sigpipe" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1rnqcgbx2mv3w11y6vf05a8a3y6jyqwmwa0hhafi6j6kw2rvz12m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "https://www.github.com/kurtbuilds/sigpipe")
    (synopsis
     "single function call to reset SIGPIPE and fix `failed printing to stdout`")
    (description
     "This package provides a single function call to reset SIGPIPE and fix `failed
printing to stdout`.")
    (license license:expat)))

(define-public rust-notify-types-2
  (package
    (name "rust-notify-types")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "notify-types" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0pcjm3wnvb7pvzw6mn89csv64ip0xhx857kr8jic5vddi6ljc22y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-serde" ,rust-serde-1)
                       ("rust-web-time" ,rust-web-time-1))))
    (home-page "https://github.com/notify-rs/notify")
    (synopsis "Types used by the notify crate")
    (description "This package provides Types used by the notify crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-bitflags-2
  (package
    (name "rust-bitflags")
    (version "2.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "bitflags" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0dixc6168i98652jxf0z9nbyn0zcis3g6hi6qdr7z5dbhcygas4g"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-bytemuck" ,rust-bytemuck-1)
                       ("rust-compiler-builtins" ,rust-compiler-builtins-0.1)
                       ("rust-rustc-std-workspace-core" ,rust-rustc-std-workspace-core-1)
                       ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/bitflags/bitflags")
    (synopsis "macro to generate structures which behave like bitflags.")
    (description
     "This package provides a macro to generate structures which behave like bitflags.")
    (license (list license:expat license:asl2.0))))

(define-public rust-notify-8
  (package
    (name "rust-notify")
    (version "8.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "notify" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0hz9ab68gsbkidms6kgl4v7capfqjyl40vpfdarcfsnnnc1q9vig"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-2)
                       ("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-filetime" ,rust-filetime-0.2)
                       ("rust-fsevent-sys" ,rust-fsevent-sys-4)
                       ("rust-inotify" ,rust-inotify-0.11)
                       ("rust-kqueue" ,rust-kqueue-1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-mio" ,rust-mio-1)
                       ("rust-notify-types" ,rust-notify-types-2)
                       ("rust-walkdir" ,rust-walkdir-2)
                       ("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page "https://github.com/notify-rs/notify")
    (synopsis "Cross-platform filesystem notification library")
    (description
     "This package provides Cross-platform filesystem notification library.")
    (license license:cc0)))

(define-public rust-redox-users-0.5
  (package
    (name "rust-redox-users")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "redox_users" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0awxx66izdw6kz97r3zxrl5ms5f6dqi5l0f58mlsvlmx8wyrsvyx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-getrandom" ,rust-getrandom-0.2)
                       ("rust-libredox" ,rust-libredox-0.1)
                       ("rust-rust-argon2" ,rust-rust-argon2-0.8)
                       ("rust-thiserror" ,rust-thiserror-2)
                       ("rust-zeroize" ,rust-zeroize-1))))
    (home-page "https://gitlab.redox-os.org/redox-os/users")
    (synopsis "Rust library to access Redox users and groups functionality")
    (description
     "This package provides a Rust library to access Redox users and groups
functionality.")
    (license license:expat)))

(define-public rust-dirs-sys-0.5
  (package
    (name "rust-dirs-sys")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "dirs-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1aqzpgq6ampza6v012gm2dppx9k35cdycbj54808ksbys9k366p0"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-option-ext" ,rust-option-ext-0.2)
                       ("rust-redox-users" ,rust-redox-users-0.5)
                       ("rust-windows-sys" ,rust-windows-sys-0.59))))
    (home-page "https://github.com/dirs-dev/dirs-sys-rs")
    (synopsis
     "System-level helper functions for the dirs and directories crates")
    (description
     "This package provides System-level helper functions for the dirs and directories crates.")
    (license (list license:expat license:asl2.0))))

(define-public rust-dirs-6
  (package
    (name "rust-dirs")
    (version "6.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "dirs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0knfikii29761g22pwfrb8d0nqpbgw77sni9h2224haisyaams63"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-dirs-sys" ,rust-dirs-sys-0.5))))
    (home-page "https://github.com/soc/dirs-rs")
    (synopsis
     "tiny low-level library that provides platform-specific standard locations of directories for config, cache and other data on Linux, Windows, macOS and Redox by leveraging the mechanisms defined by the XDG base/user directory specifications on Linux, the Known Folder API on Windows, and the Standard Directory guidelines on macOS.")
    (description
     "This package provides a tiny low-level library that provides platform-specific
standard locations of directories for config, cache and other data on Linux,
Windows, @code{macOS} and Redox by leveraging the mechanisms defined by the XDG
base/user directory specifications on Linux, the Known Folder API on Windows,
and the Standard Directory guidelines on @code{macOS}.")
    (license (list license:expat license:asl2.0))))

(define-public rust-codex-0.1
  (package
    (name "rust-codex")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "codex" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0dj0hqw3wk5p3k77zi1sczds00d6mdwhwdb4w7jh1drqxsh2fkbj"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/typst/codex")
    (synopsis "Human-friendly notation for Unicode symbols")
    (description
     "This package provides Human-friendly notation for Unicode symbols.")
    (license license:asl2.0)))

(define-public rust-two-face-0.4
  (package
    (name "rust-two-face")
    (version "0.4.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "two-face" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0lpqra6ryq4q47iavmcabbgxknajv59485wsyg3f4qnzim1xlkiq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-once-cell" ,rust-once-cell-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-syntect" ,rust-syntect-5))))
    (home-page "https://github.com/CosmicHorrorDev/two-face")
    (synopsis "Extra syntax and theme definitions for syntect")
    (description
     "This package provides Extra syntax and theme definitions for syntect.")
    (license (list license:expat license:asl2.0))))
