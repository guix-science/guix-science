;;;
;;; Copyright © 2022-2025 Emmanuel Medernach <Emmanuel.Medernach@iphc.cnrs.fr>
;;; Copyright © 2023-2025 Jake Forster <jakecameron.forster@gmail.com>
;;; Copyright © 2025 Lars Bilke <lars.bilke@ufz.de>
;;; Copyright © 2025 Romain Garbage <romain.garbage@inria.fr>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-science packages physics)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (ice-9 string-fun)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages kerberos)
  #:use-module (ice-9 match)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages digest)
  #:use-module (gnu packages tbb)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages astronomy)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages machine-learning)
  #:use-module (guix utils))

(define (replace-char old-char new-char str)
  (string-map (lambda (char)
                (if (char=? char old-char) new-char char)) str))

(define-public clhep-2.4.6.2
  (package
    (name "clhep")
    (version "2.4.6.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.cern.ch/CLHEP/CLHEP")
             (commit (string-append "CLHEP_"
                                    (replace-char #\. #\_ version)))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "06mpvpb5ywx8j5cpddh9sb415h02mim3ip1sd895q94pxci2jr92"))))
    (build-system cmake-build-system)
    (home-page "https://proj-clhep.web.cern.ch/proj-clhep/")
    (synopsis "HEP-specific foundation and utility classes")
    (description
     "CLHEP is a set of HEP-specific foundation and utility classes such as
random generators, physics vectors, geometry and linear algebra.
CLHEP is structured in a set of packages independent of any external
package.")
    (license license:gpl3+)))

(define-public clhep-2.4.7.1
  (package
    (inherit clhep-2.4.6.2)
    (version "2.4.7.1")
    (source
     (origin
       (inherit (package-source clhep-2.4.6.2))
       (uri (git-reference
             (url "https://gitlab.cern.ch/CLHEP/CLHEP")
             (commit (string-append "CLHEP_"
                                    (replace-char #\. #\_ version)))))
       (sha256
        (base32 "13la49jnadsi5f2a70qagv87j7qzcpdccmbp2lyscaxlkmzy253z"))))))

(define-public clhep clhep-2.4.7.1)

(define-public davix
  (package
   (name "davix")
   (version "0.8.7")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/cern-fts/davix")
           (commit (string-append "R_" (string-replace-substring version "." "_")))
           (recursive? #t))) ; submodules
     (file-name (string-append name "-" version))
     (sha256
      (base32 "1w0alvkhc23iy42kqrb543jja4v6xs7z600vhn890hprj3fmpis2"))))
   (build-system cmake-build-system)
   (native-inputs (list python))
   (inputs
    (list `(,util-linux "lib") ; For <uuid/uuid.h>
          curl
          libxml2
          openssl))
   (arguments
    (list #:configure-flags
          #~(list "-DEMBEDDED_LIBCURL=FALSE"
                  "-DLIBCURL_BACKEND_BY_DEFAULT=TRUE")))
   (home-page "https://davix.web.cern.ch/")
   (synopsis "Manage files over HTTP-based protocols")
   (description
    "Davix aims to make the task of managing files
overHTTP-based protocols simple.  It is being developed by
the IT-SDC-ID section at CERN, and while the project’s
purpose is its use on the CERN grid, the functionality
offered is generic.")
   (license license:lgpl2.1+)))

(define-public dcap
  (package
    (name "dcap")
    (version "2.47.14")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dCache/dcap")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0ihbmqrz5vrad09n0njd0y26jn590kxm3zgphqa2avf8aj82fzl6"))))
    (build-system gnu-build-system)
    (native-inputs (list automake autoconf cunit libtool))
    (inputs (list libxcrypt zlib))
    (home-page "https://github.com/dCache/dcap")
    (synopsis "POSIX like interface for accessing dCache")
    (description "DCAP (dCache access protocol) client library: DCAP is
the native random access I/O protocol for files within
dCache.  In addition to the usual data transfer mechanisms,
it supports all necessary file metadata and name space
manipulation operations.")
    (license license:lgpl2.1+)))

(define-public tfel
  (package
    (name "tfel")
    (version "4.2.2")  ;; Keep in sync with compatible version of mgis
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/thelfer/tfel")
              (commit (string-append "TFEL-" version))))
        (file-name (git-file-name name version))
        (sha256
        (base32 "14br7n76qfh651hvmn1i0ma5lr5ayhvj4ay2182isx26m1j15cfz"))))
    (build-system cmake-build-system)
    (arguments
      (list #:configure-flags #~(list "-Denable-portable-build=ON")
            #:phases #~(modify-phases %standard-phases
                         ;; Test targets are not build by default. By building these
                         ;; additional targets the majority of tests (over 6000) can
                         ;; be run.
                         (add-after 'build 'build-test-targets
                           (lambda* (#:key tests? parallel-build?
                                     #:allow-other-keys)
                             (if tests?
                                 (invoke "make"
                                         "MFrontGenericBehaviours"
                                         "MFrontGenericBehaviours2"
                                         "MFrontGenericBehaviours3"
                                         "-j"
                                         (if parallel-build?
                                             (number->string (parallel-job-count))
                                             "1"))
                                 (format #t "test suite not run~%"))))
                         (replace 'check
                           (lambda* (#:key tests? parallel-tests?
                                     #:allow-other-keys)
                             (if tests?
                                 (invoke "ctest"
                                         "-R"
                                         "generic"
                                         "-E"
                                         "brick"
                                         "-j"
                                         (if parallel-tests?
                                             (number->string (parallel-job-count))
                                             "1"))
                                 (format #t "test suite not run~%")))))))
    (home-page "https://thelfer.github.io/tfel/web/index.html")
    (synopsis "TFEL library and MFront code generator")
    (description
      "MFront is a code generator which translates a set of closely
related domain specific languages into plain C++ on top of the TFEL library.")
    ;; TFEL/MFront is released under either the GNU GPL licence with linking
    ;; exception or the CECILL-A licence:
    (license (list license:gpl3+ license:cecill))))

(define-public mgis
  (package
    (name "mgis")
    (version "2.2")  ;; Keep in sync with compatible version of tfel
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/thelfer/MFrontGenericInterfaceSupport")
              (commit (string-append "MFrontGenericInterfaceSupport-" version))))
        (file-name (git-file-name name version))
        (sha256
        (base32 "00ij7gaqrzakvc3n6irq5z5b5nd08kik5i87prvnp9604ssa6h8k"))))
    (build-system cmake-build-system)
    (arguments
      (list #:configure-flags #~(list "-Denable-doxygen-doc=OFF"
                                      "-Denable-portable-build=ON")
            #:phases #~(modify-phases %standard-phases
                         ;; Test targets are not build by default. By building these
                         ;; additional targets some tests can be run.
                         (add-after 'build 'build-test-targets
                           (lambda* (#:key tests? parallel-build?
                                     #:allow-other-keys)
                             (if tests?
                                 (invoke "make"
                                  "BehaviourTest"
                                  "MFrontGenericBehaviourInterfaceTest"
                                  "MFrontGenericBehaviourInterfaceTest2"
                                  "MFrontGenericBehaviourInterfaceTest3"
                                  "-j"
                                  (if parallel-build?
                                      (number->string (parallel-job-count))
                                      "1"))
                                 (format #t "test suite not run~%"))))
                         (replace 'check
                           (lambda* (#:key tests? parallel-tests?
                                     #:allow-other-keys)
                             (if tests?
                                 (invoke "ctest" "-R"
                                  "MFrontGenericBehaviourInterfaceTest" "-j"
                                  (if parallel-tests?
                                      (number->string (parallel-job-count))
                                      "1"))
                                 (format #t "test suite not run~%")))))))
    (inputs (list tfel))
    (home-page "https://thelfer.github.io/mgis/web/index.html")
    (synopsis
      "MFrontGenericInterfaceSupport provides tools to handle MFront behaviours")
    (description
      "Those tools are meant to be used by solver developers to e.g. load
MFront behaviours from external shared libraries and retrieve all relevant
meta data function.")
    ;; MFrontGenericInterfaceSupport is released under either the GNU LGPL license
    ;; or the CECILL-C license:
    (license (list license:lgpl3 license:cecill-c))))

(define-public smilei
  (package
    (name "smilei")
    (version "5.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/SmileiPIC/Smilei")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1r1hr7flprjgyy3vnyk851fmkwrwimxbwj3krx4gaa5217590ym8"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   ;; No configure script.
                   (delete 'configure)
                   ;; No tests provided, so they are implemented in the
                   ;; package definition.
                   (replace 'check
                     (lambda* (#:key tests? #:allow-other-keys)
                       (when tests?
                         (invoke "./smilei" "benchmarks/tst_quick_1d.py"))))
                   ;; No install target in the Makefile.
                   (replace 'install
                     (lambda _
                       (let ((bin (string-append #$output "/bin")))
                         (install-file "smilei" bin)
                         (install-file "smilei_test" bin)))))))
    (inputs (list hdf5-parallel-openmpi libomp openmpi python-wrapper))
    (native-inputs (list util-linux python-numpy))
    (properties '((tunable? . #t)))
    (home-page "https://smileipic.github.io/Smilei/")
    (synopsis "Particle-in-cell code for plasma simulation")
    (description
     "Smilei is a user-friendly electromagnetic particle-in-cell code for
the kinetic simulation of plasmas.  Co-developed by physicists and
computer scientists, it is designed for high-performance on the most
recent supercomputing architectures.  Smilei is applied to a wide range
of applications, from laser-plasma interaction, to accelerator
physics, space physics and astrophysics.")
    (license license:cecill-b)))

(define-public xrootd
  (package
    (name "xrootd")
    (version "5.7.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/xrootd/xrootd")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1k68gl48png2w78j067hmkl3mad3zx06x8z4gpn8i5vr419gsyfp"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~(list "-DENABLE_TESTS=ON")
      #:phases
      #~(modify-phases %standard-phases
          (add-before 'install 'fix-python-setup
            (lambda _
              ;; $LIB is not expanded by dynamic linker.
              (substitute* "bindings/python/setup.py"
                (("\\$LIB")
                 "lib")))))))
    (inputs
     (list zlib
           `(,util-linux "lib")         ;for libuuid
           openssl
           libxcrypt                    ;for crypt.h
           ;; These are bundled.  Use guix packages instead.
           tinyxml
           libxml2
           ;; Support optional features.
           readline
           curl                         ;for HTTP TPC
           mit-krb5
           isa-l                        ;for erasure coding
           python))
    (native-inputs
     (list googletest
           procps               ;for tests
           ;; tests/cluster/setup.sh uses uuidgen
           util-linux))
    (home-page "http://xrootd.org")
    (synopsis "Framework for fast, low latency, scalable data access")
    (description
     "XRootD aims to give high performance, scalable, fault tolerant access
to data repositories of many kinds, including file-based ones.  It is
based on a scalable architecture, a communication protocol, and a set
of plugins and tools.")
    (license license:lgpl3+)))

;; TODO: Unbundle LLVM, Clang, and Cling.
(define-public root
  (package
    (name "root")
    (version "6.34.02")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/root-project/root")
             (commit (string-append
                      "v" (string-map (match-lambda
                                        (#\. #\-)
                                        (chr chr)) version)))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1whhmrz40x3blhzxsh8h6vfs8xi6pv0a3mwndlrapnajy5p582dk"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags
      #~(list "-Dtesting=ON"
              ;; Python modules are installed in 'lib' by default.
              (string-append
               "-DCMAKE_INSTALL_PYTHONDIR="
               #$output "/lib/python"
               #$(version-major+minor (package-version python))
               "/site-packages"))
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'fix-cmake-rpath
            (lambda _
              ;; Default RPATH is '.' and '../lib', but
              ;; libROOTPythonizations.so is installed in Python
              ;; 'site-packages'. Correct its path to 'lib'.
              (substitute* "bindings/pyroot/pythonizations/CMakeLists.txt"
               (("SHARED \\$\\{cpp_sources\\}\\)")
                (string-append
                 "SHARED ${cpp_sources})\n\n"
                 "set_target_properties(ROOTPythonizations PROPERTIES "
                 "INSTALL_RPATH \"$ORIGIN;$ORIGIN/../../\")")))))
          (add-before 'build 'set-env
            (lambda _
              ;; For bundled libAfterImage configure script.
              (setenv "CONFIG_SHELL" (which "sh"))))
          (replace 'check
            (lambda* (#:key tests? parallel-tests? #:allow-other-keys)
              (when tests?
                (let ((skipped-tests
                       (list
                        ;; FIXME: This test failure may be fixed in 6.36.00.
                        ;; https://github.com/root-project/root/issues/17564
                        "gtest-roofit-roofit-vectorisedPDFs-testLandau"
                        ;; Skip tests that require python-tensorflow
                        ;; due to limited availability of substitutes
                        ;; for that package from guix-science.
                        "test-import-tensorflow"
                        "pyunittests-bindings-pyroot-pythonizations-batchgen"
                        ;; Skip tests that require network.
                        "gtest-io-io-TFile"
                        "gtest-net-netxng-RRawFileNetXNG"
                        "gtest-net-netxng-TNetXNGFileTest"
                        "test-stressgeometry.*"
                        "test-stresslinear.*"
                        "test-stresshistogram.*"
                        "test-stressIOPlugins-xroot"
                        "PyMVA-RandomForest-Classification"
                        "PyMVA-GTB-Classification"
                        "PyMVA-AdaBoost-Classification"
                        "PyMVA-Torch-Classification"
                        "PyMVA-Torch-Regression"
                        "gtest-tmva-tmva-rstandardscaler"
                        "gtest-tmva-tmva-rreader"
                        "test-stresstmva"
                        "^TMVA-DNN-MethodDL-.*"
                        "^gtest-tmva-tmva-envelope-TMVA-.*"
                        "gtest-tree-tree-testTChainParsing"
                        "gtest-tree-treeplayer-treeprocessormt-remotefiles"
                        "tutorial-dataframe-df014_CSVDataSource"
                        "tutorial-dataframe-df015_LazyDataSource"
                        "tutorial-dataframe-df101_h1Analysis"
                        "tutorial-graphics-AtlasExample"
                        "tutorial-hist-th2polyEurope"
                        "tutorial-hist-th2polyUSA"
                        "tutorial-multicore-imt001_parBranchProcessing"
                        "tutorial-multicore-imt101_parTreeProcessing"
                        "tutorial-multicore-mp103_processSelector"
                        "tutorial-multicore-mp104_processH1"
                        "tutorial-multicore-mp105_processEntryList"
                        "tutorial-tmva-TMVAClassification"
                        "tutorial-tmva-TMVAClassificationApplication"
                        "tutorial-tmva-TMVACrossValidationRegression"
                        "tutorial-tmva-TMVAMulticlass"
                        "tutorial-tmva-TMVAMulticlassApplication"
                        "tutorial-tmva-TMVARegression"
                        "tutorial-tmva-TMVARegressionApplication"
                        "tutorial-tmva-TMVA_Higgs_Classification"
                        "tutorial-tmva-envelope-classification"
                        "tutorial-tmva-tmva003_RReader"
                        "tutorial-tmva-tmva004_RStandardScaler"
                        "tutorial-tree-run_h1analysis"
                        "tutorial-v7-ntuple-ntpl008_import"
                        "tutorial-dataframe-df014_CSVDataSource-py"
                        "tutorial-dataframe-df033_Describe-py"
                        "tutorial-dataframe-df102_NanoAODDimuonAnalysis-py"
                        "tutorial-dataframe-df103_NanoAODHiggsAnalysis-py"
                        "tutorial-dataframe-df104_HiggsToTwoPhotons-py"
                        "tutorial-dataframe-df105_WBosonAnalysis-py"
                        "tutorial-dataframe-df106_HiggsToFourLeptons-py"
                        "tutorial-dataframe-df107_SingleTopAnalysis-py"
                        "tutorial-rcanvas-df104-py"
                        "tutorial-rcanvas-df105-py"
                        "tutorial-roofit-rf618_mixture_models-py"
                        "tutorial-tmva-RBatchGenerator_NumPy-py"
                        "tutorial-tmva-RBatchGenerator_PyTorch-py"
                        "tutorial-tmva-TMVA_Higgs_Classification-py"
                        "tutorial-tmva-pytorch-ApplicationClassificationPyTorch-py"
                        "tutorial-tmva-pytorch-ApplicationRegressionPyTorch-py"
                        "tutorial-tmva-pytorch-ClassificationPyTorch-py"
                        "tutorial-tmva-pytorch-RegressionPyTorch-py"
                        "tutorial-tmva-tmva100_DataPreparation-py"
                        "tutorial-tmva-tmva101_Training-py"
                        "tutorial-tmva-tmva102_Testing-py")))
                  (invoke "ctest" "-E"
                          (string-append
                           "^(" (string-join skipped-tests "|") ")$")
                          "-j"
                          (if parallel-tests?
                              (number->string (parallel-job-count))
                              "1"))))))
          (add-after 'install 'fix-python-so-loading
            (lambda _
              ;; ctypes.CDLL fails to load shared library from
              ;; absolute path to symlink.
              (substitute*
                  (string-append
                   #$output "/lib/python"
                   #$(version-major+minor (package-version python))
                   "/site-packages/cppyy_backend/loader.py")
                (("if not c:")
                 (string-append "c = _load_helper(\""
                                #$output
                                "/lib/libcppyy_backend.so\")"
                                "\n\n    if not c:"))))))))
    (inputs
     (list libx11
           libxpm
           libxft
           libxext
           python
           libxcrypt
           xrootd
           ;; These are bundled.  Use guix packages instead.
           pcre2
           xxhash
           `(,zstd "lib")
           lz4
           ;; Support optional features.
           tbb                          ;for imt
           openblas                     ;for tmva-cpu
           python-numpy                 ;for tmva-pymva
           glew                         ;for opengl
           gl2ps                        ;instead of bundled
           ftgl                         ;instead of bundled
           libxml2
           giflib
           libtiff
           libjpeg-turbo
           openssl
           mysql
           postgresql
           sqlite
           cfitsio))
    (native-inputs
     (list googletest
           python-pandas                ;for tests
           python-numba                 ;for tests
           python-pytorch               ;for tests
           python-scikit-learn          ;for tests
           python-xgboost))             ;for tests
    (home-page "https://root.cern")
    (synopsis "Data analysis framework made at CERN")
    (description
     "ROOT is a data analysis framework developed by CERN for tasks such
as data storage, processing, and visualization.  It provides tools for
histograms, statistical tests, fitting, simulations, and machine
learning.  It can handle large datasets and uses a specialized file
format.")
    (license license:lgpl2.1+)))

(define-public vdt
  (package
    (name "vdt")
    (version "0.4.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dpiparo/vdt")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0biqp68k5qg5zy2nbi44f19vqyk2nma5sa00yf47ihy120p6as3l"))))
    (build-system cmake-build-system)
    (native-inputs (list python))
    (arguments
     `(#:tests? #f)) ;There are no tests
    (home-page "https://root.cern/root/html606/md_math_vdt_ReadMe.html")
    (synopsis "The VDT mathematical library")
    (description
     "VDT is a library of mathematical functions,
implemented in double and single precision.  The
implementation is fast and with the aid of modern
compilers (e.g. gcc 4.7) vectorisable.  VDT exploits also
Pade polynomials.")
    (license license:lgpl3+)))
