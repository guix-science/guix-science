;;;
;;; Copyright © 2024 Romain Garbage <romain.garbage@inria.fr>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-science packages forgejo)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

;; This package is based on a binary distribution. While the policy is
;; to build the software from source, this package depends on a (soft)
;; fork of https://github.com/nektos/act, which causes guix import -r
;; to add 450+ new package definitions, adding a burden to
;; maintainability.
(define-public forgejo-runner
  (package
    (name "forgejo-runner")
    (version "5.0.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://code.forgejo.org/forgejo/runner/releases/download/v5.0.4/"
             name "-" version "-linux-amd64.xz"))
       (sha256
        (base32 "055lvqi28dzh7f774hw57cdksy9xskf26fvgfzfqjrr4rbd6xdg4"))))
    (build-system gnu-build-system)
    (arguments
     (list #:tests? #f ;; Binary distribution: no tests.
           #:phases
           #~(modify-phases %standard-phases
               ;; No configure script.
               (delete 'configure)
               (replace 'build
                 (lambda _
                   (let ((source-name (format #f "~a-~a-linux-amd64"
                                              #$(package-name this-package)
                                              #$(package-version this-package)))
                         (target-name #$(package-name this-package)))
                     (rename-file source-name target-name)
                     (chmod target-name #o555))))
               (replace 'install
                 (lambda _
                   (install-file #$(package-name this-package)
                                 (string-append #$output "/bin")))))))
    ;; Source is a prebuilt binary.
    (supported-systems (list "x86_64-linux"))
    (home-page "https://forgejo.org")
    (synopsis "Daemon that fetches workflows to run from a Forgejo instance")
    (description "The Forgejo Runner is a daemon that fetches workflows to run from a
Forgejo instance, executes them, sends back with the logs and
ultimately reports its success or failure.

This package uses the binary release of the runner and is *not* built
from source.")
    (license license:bsd-1)))
