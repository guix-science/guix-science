;;;
;;; Copyright © 2016-2021 Roel Janssen <roel@gnu.org>
;;; Copyright © 2025 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guix-science packages grid-engine)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages dbm)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages java)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages onc-rpc)
  #:use-module (gnu packages parallel)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages tls))

(define-public grid-engine-core
  (package
    (name "grid-engine-core")
    (version "8.1.9")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://arc.liv.ac.uk/downloads/SGE/releases/"
                    version "/sge-" version ".tar.gz"))
              (sha256
               (base32
                "0ra7m9mf09zzcf815y3zqzqkj95v9zm24nhhvzmdh2bsqgdmk59w"))
              (patches (search-patches "grid-engine-core-openssl-1.1.patch"
                                       "grid-engine-core-extern_qualifier.patch"))))
    (build-system gnu-build-system)
    (supported-systems '("x86_64-linux"))
    (inputs
     (list bdb
           coreutils
           `(,hwloc "lib")
           gawk
           icedtea-8
           inetutils
           libtirpc
           libxcrypt
           linux-pam
           openssl
           python-2.7
           perl
           ruby
           tcl
           tcsh))
    (arguments
     (list
      #:tests? #f ;there is no check target
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'patch-various-stuff
            (lambda* (#:key inputs #:allow-other-keys)
              (substitute* "source/aimk"
                (("/usr/bin/uname") "uname")
                (("uname") (search-input-file inputs "/bin/uname")))
              (substitute* "source/dist/util/arch"
                (("/bin/uname")
                 (search-input-file inputs "/bin/uname"))
                (("/lib64/libc.so.6")
                 (search-input-file inputs "/lib/libc.so.6"))
                (("awk") (search-input-file inputs "/bin/gawk"))
                (("head") (search-input-file inputs "/bin/head")))
              (substitute* "source/aimk"
                (("= cc") (string-append "= gcc")))
              (substitute* "source/configure"
                (("SHELL=")
                 (string-append "SHELL=" (assoc-ref inputs "bash")
                                "/bin/sh #")))))
          (replace 'configure
            (lambda* (#:key inputs #:allow-other-keys)
              (chdir "source")
              (setenv "SGE_INPUT_LDFLAGS" "-ltirpc")
              (setenv "SGE_INPUT_CFLAGS"
                      (string-append
                       "-I" #$(this-package-input "openssl") "/include "
                       "-I" #$(this-package-input "libtirpc") "/include/tirpc"))
              (setenv "JAVA_HOME" #$(this-package-input "icedtea"))
              (system "scripts/bootstrap.sh")))
          (replace 'build
            (lambda _
              (system "./aimk -only-core -no-java -no-jni")))
          (replace 'install
            (lambda* (#:key outputs #:allow-other-keys)
              ;; The scripts/distinst would not work, so we copy the files
              ;; over manually.
              (chdir "LINUXAMD64")
              (let ((bin (string-append #$output "/bin"))
                    (lib (string-append #$output "/lib"))
                    (include (string-append #$output "/include")))
                (mkdir-p bin)
                (mkdir-p lib)
                (mkdir-p include)

                ;; Binaries
                (for-each (lambda (file)
                            (install-file file bin))
                          '("qacct" "qalter" "qconf" "qdel" "qevent" "qhost"
                            "qmod" "qping" "qquota" "qrdel" "qrstat" "qrsub"
                            "qsh" "qstat" "qsub" "sge_coshepherd" "sge_execd"
                            "sgepasswd" "sge_qmaster" "sge_shadowd"
                            "sge_share_mon" "sge_shepherd"))
                (symlink (string-append bin "/qalter")
                         (string-append bin "/qhold"))
                (symlink (string-append bin "/qalter")
                         (string-append bin "/qresub"))
                (symlink (string-append bin "/qalter")
                         (string-append bin "/qrls"))
                (symlink (string-append bin "/qsh")
                         (string-append bin "/qrsh"))
                (symlink (string-append bin "/qstat")
                         (string-append bin "/qselect"))
                (symlink (string-append bin "/qsh")
                         (string-append bin "/qlogin"))

                ;; Libraries
                (for-each (lambda (file)
                            (install-file file lib))
                          '("libdrmaa.so" "libjuti.so" "libspoolb.so"
                            "libspoolc.so" "pam_sge_authorize.so"
                            "pam_sge-qrsh-setup.so"))
                (symlink (string-append lib "/libdrmaa.so")
                         (string-append lib "/libdrmaa.so.1"))
                (symlink (string-append lib "/libdrmaa.so")
                         (string-append lib "/libdrmaa.so.1.0"))

                ;; Headers
                (install-file "../libs/japi/drmaa.h" include)
                (install-file "../libs/sched/sge_pqs_api.h" include)

                ;; Pkg-config file
                (mkdir-p (string-append lib "/pkgconfig"))
                (with-output-to-file (string-append lib "/pkgconfig/drmaa.pc")
                  (lambda _
                    (format #t "Name: drmaa~%Description: DRMAA interface~%Version: 8.1.9~%Requires:~%Libs: -L~a -ldrmaa~%Cflags: -I~a" lib include)))))))))
    (native-search-paths
     (list (search-path-specification
            (variable "LD_LIBRARY_PATH")
            (files '("lib")))
           (search-path-specification
            (variable "DRMAA_LIBRARY_PATH")
            (files '("lib/libdrmaa.so"))
            (file-type 'regular))))
    (home-page "https://arc.liv.ac.uk/trac/SGE")
    (synopsis "Implementation of a grid engine")
    (description "The Son of Grid Engine is a community project to continue
Sun's old gridengine project after Oracle shut down the site and stopped
contributing code.")
    (license (list license:asl2.0 license:gpl2+))))

(define-public qsub-slurm
  (let ((commit "1321c3bdaacf4b37b3bc20ed124fc88c1dfb38ef"))
    (package
     (name "qsub-slurm")
     (version "0.0.7")
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/roelj/qsub-slurm.git")
                    (commit commit)))
              (sha256
               (base32
                "0hm7c83ih1579nppc4s1s78ad3dzqjv7wzajzpqi1gr0gkn0566c"))))
     (build-system gnu-build-system)
     (arguments
      `(#:tests? #f))
     (native-inputs
      (list autoconf automake pkg-config))
     (inputs
      (list slurm guile-3.0))
     (home-page "https://github.com/roelj/qsub-slurm")
     (synopsis "Compatibility tool to move from SGE to SLURM.")
     (description "This package an alternative @code{qsub} command that
will submit jobs to SLURM.")
     (license license:gpl3+))))

(define-public qsub-local
  (let ((commit "3959ef2ed2e559798d8e7928579816c13404381b"))
    (package
     (name "qsub-local")
     (version "0.0.1")
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/roelj/qsub-local.git")
                    (commit commit)))
              (sha256
               (base32
                "1dxp3s311397jf6lm6jdik8fwx56kbghx5v97nb6kjz33ymycy9p"))))
     (build-system gnu-build-system)
     (arguments
      `(#:tests? #f))
     (native-inputs
      (list autoconf automake pkg-config))
     (inputs
      (list bash guile-3.0))
     (home-page "https://github.com/roelj/qsub-local")
     (synopsis "Compatibility tool to run SGE pipelines locally.")
     (description "This package an alternative @code{qsub} command that
will directory run the script.")
     (license license:gpl3+))))
